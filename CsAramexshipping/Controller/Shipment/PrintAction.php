<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_CsAramexShipping
 * @author        CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (https://cedcommerce.com/)
 * @license      https://cedcommerce.com/license-agreement.txt
 */

namespace Ced\CsAramexShipping\Controller\Shipment;

/**
 * Class PrintAction
 * @package Ced\CsAramexShipping\Controller\Shipment
 */
class PrintAction extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\Module\Dir\Reader
     */
    protected $_moduleReader;

    /**
     * @var \Magento\Sales\Model\Order\ShipmentFactory
     */
    protected $shipmentFactory;

    /**
     * PrintAction constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Module\Dir\Reader $moduleReader
     * @param \Magento\Sales\Model\Order\ShipmentFactory $shipmentFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        \Magento\Sales\Model\Order\ShipmentFactory $shipmentFactory
    )
    {
        parent::__construct($context);
        $this->_scopeConfig = $scopeConfig;
        $this->_moduleReader = $moduleReader;
        $this->shipmentFactory = $shipmentFactory;
    }

    /**
     * Default track shipment page
     *
     * @return void
     */
    public function execute()
    {
        $shipment_id = $this->getRequest()->getParam('id');
        $shipment = $this->shipmentFactory->create()->load($shipment_id);
        foreach ($shipment->getAllTracks() as $tracknum) {
            $awb = $tracknum->getNumber();
        }
        $account_number = $this->_scopeConfig->getValue('carriers/aramexshipping/account_number',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $account_country_code = $this->_scopeConfig->getValue('carriers/aramexshipping/account_country_code',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $account_entity = $this->_scopeConfig->getValue('carriers/aramexshipping/account_entity',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $account_pin = $this->_scopeConfig->getValue('carriers/aramexshipping/account_pin',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $account_username = $this->_scopeConfig->getValue('carriers/aramexshipping/username',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $account_password = $this->_scopeConfig->getValue('carriers/aramexshipping/password',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $params = [
            'ClientInfo' => [
                'AccountCountryCode' => $account_country_code,
                'AccountEntity' => $account_entity,
                'AccountNumber' => $account_number,
                'AccountPin' => $account_pin,
                'UserName' => $account_username,
                'Password' => $account_password,
                'Version' => 'v1.0'
            ],
            'Transaction' => [
                'Reference1' => '001',
                'Reference2' => '',
                'Reference3' => '',
                'Reference4' => '',
                'Reference5' => '',
            ],
            'LabelInfo' => [
                'ReportID' => '9201',
                'ReportType' => 'URL',
            ],
        ];
        $params['ShipmentNumber'] = $awb;
        $wsdlPath = $this->_moduleReader->getModuleDir('etc', 'Ced_Aramexshipping') . '/' . 'wsdl';
        $wsdl = $wsdlPath . '/' . 'shipping-services-api-wsdl.wsdl';
        $soapClient = new \SoapClient($wsdl);
        try {
            $auth_call = $soapClient->PrintLabel($params);
            if ($auth_call->HasErrors) {
                die('aaa');
            }
            $shipment_id = $this->getRequest()->getParam('id');
            $shipment = $this->shipmentFactory->create()->load($shipment_id);
            $_order = $shipment->getOrder();
            $filepath = $auth_call->ShipmentLabel->LabelURL;
            $name = "{$_order->getIncrementId()}-shipment-label.pdf";
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $name . '"');
            header("HTTP/1.1 301 Moved Permanently");
            header('Location: ' . $filepath);
            exit();
        } catch (\Exception $e) {
            die($e);
        }
    }
}