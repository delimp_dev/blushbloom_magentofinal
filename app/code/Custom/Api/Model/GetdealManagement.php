<?php


namespace Custom\Api\Model;

/**
 * Class GetdealManagement
 *
 * @package Custom\Api\Model
 */
class GetdealManagement implements \Custom\Api\Api\GetdealManagementInterface
{

    /**
     * {@inheritdoc}
     */
    public function getGetdeal($param)
    {

    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
		$mediaUrl = $objectManager->get('Magento\Store\Model\StoreManagerInterface')
                    ->getStore()
                    ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
		$imageUrl = $mediaUrl.'catalog/product';
		$productCollectionFactory = $objectManager->get('\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
		$collection = $productCollectionFactory->create();
		$collection->addAttributeToSelect('*')->addAttributeToFilter('special_price', ['neq' => '']);
		$productData = [];
		$productData1 = [];
		foreach ($collection as $productItem) {
			$entity_id = $productItem->getEntityId();
			$productName = $productItem->getName();
			$productThumbNail = $productItem->getThumbnail();
			$productImage = $productItem->getImage();
			$productUrlKey = $productItem->getUrlKey();
			$productSale = $productItem->getSale();
			$productSpecialToDate = $productItem->getSpecialToDate();
			$specialPrice = $productItem->getSpecialPrice();
			$price = $productItem->getPrice();
			$productData['entity_id'] = $entity_id;
			$productData['name'] = $productName;
			$productData['thumbnail'] = $productThumbNail;
			$productData['image'] = $productImage;
			$productData['url'] = $productUrlKey;
			$productData['sale'] = $productSale;
			$productData['productSpecialToDate'] = $productSpecialToDate;
			$curdate = strtotime(date("Y-m-d"));
			$mydate = strtotime($productSpecialToDate);
			
			
			$productData['specialPrice'] = $specialPrice;
			$productData['price'] = $price;
			if ($mydate > $curdate) {
				array_push($productData1, $productData);
			}
			
		}
        return $productData1;
    }
}

