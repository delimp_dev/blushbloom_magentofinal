<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_Aramexshipping
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

namespace Ced\CsAramexshipping\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class SalesShipment implements ObserverInterface
{
     protected $_registry = null;
     protected $_storeManager;
     protected $_moduleReader;

   public function __construct (        
        \Magento\Framework\Registry $registry,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        \Magento\Customer\Model\Session $customerSession,
        RequestInterface $request
    ) {
        $this->_objectManager=$objectManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_registry = $registry;
        $this->_storeManager = $storeManager;
        $this->_moduleReader = $moduleReader;
        $this->_session = $customerSession;
        $this->_request = $request;
    }
 
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
    	$shipment = $observer->getEvent()->getShipment ();
		$order = $shipment->getOrder ();
		$cod_amount = 0;
		
		$shippingMethod = $order->getShippingMethod ();
		$s = explode ( "~", $shippingMethod );
		$shippingMethod = $s [0];
		$postData = $this->_request->getPostValue();

		if (strpos($shippingMethod, 'aramexshipping') !== false) {
		    $wsdlPath = $this->_moduleReader->getModuleDir('etc', 'Ced_Aramexshipping') . '/'. 'wsdl';
            $wsdl = $wsdlPath . '/' . 'shipping-services-api-wsdl.wsdl';

            $account_number = $this->_scopeConfig->getValue('carriers/aramexshipping/account_number', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$account_country_code = $this->_scopeConfig->getValue('carriers/aramexshipping/account_country_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$account_entity = $this->_scopeConfig->getValue('carriers/aramexshipping/account_entity', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$account_pin = $this->_scopeConfig->getValue('carriers/aramexshipping/account_pin', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$account_username = $this->_scopeConfig->getValue('carriers/aramexshipping/username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$account_password = $this->_scopeConfig->getValue('carriers/aramexshipping/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$weight_unit = $this->_scopeConfig->getValue('carriers/aramexshipping/unit_of_measure', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$vendor = $this->_session->getVendor();
			
			$vendorId = $this->_session->getVendorId();
			$vsetting = $this->_objectManager->create('Ced\CsMarketplace\Model\Vsettings')->getCollection()->addFieldToFilter('vendor_id',$vendorId); 
			
			foreach($vsetting as $key=> $value){
	            if($value['key'] == 'shipping/address/postcode')
	    			$store_zip = $value['value'];
	    		if($value['key'] == 'shipping/address/city')
	    			$city = $value['value'];
	    		if($value['key'] == 'shipping/address/country_id')
	    			$store_country = $value['value'];		    			 
	    		if($value['key'] == 'shipping/address/city')		    	
	    			$store_city=$value['value'];
	        } 
			
			$store_street1 = $this->_scopeConfig->getValue('shipping/origin/street_line1', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$store_street2 = $this->_scopeConfig->getValue('shipping/origin/street_line2', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

			


			// customer details
			$shippingaddress = $order->getShippingAddress ();
			$customer_country = $shippingaddress->getCountryId ();
			$customer_postcode = $shippingaddress->getPostcode ();
			$customer_city = $shippingaddress->getCity ();
			$changed_city = $this->_objectManager->create("Ced\City\Model\City")->load($customer_city, 'city_code')->getCityAramex();
			if($changed_city){
				$customer_city = $changed_city;
			}
			$items = $this->_objectManager->create("Magento\Sales\Model\Order\Item")->getCollection()->addFieldToFilter('order_id',$order->getEntityId())->addFieldToFilter('vendor_id',$vendorId);
			$totalWeight = 0;
			$totalItems = 0;
			$totalPrice = 0;
			$description = '';
			if($store_country == $customer_country){
				$product_group = 'DOM';
				$product_type = 'OND';
			}
			else{
				$product_group = 'EXP';
				$product_type = 'PDX';
			}

			if(isset($postData['invoice']) && count($postData['invoice']))
				$postItems = $postData['invoice']['items'];
			else
				$postItems = $postData['shipment']['items'];

			$seller_order = $this->_objectManager->create('Ced\CsMarketplace\Model\Vorders')->load($this->_request->getParam('order_id'));
			foreach($items as $item){
				if(isset($postItems[$item->getItemId()])){
					$qty = $item->getQtyOrdered();
					if($item->getWeight() != 0){
						$weight =  $item->getWeight()* $qty;
					} 
					 
					$totalWeight 	+= $weight;
					$totalItems 	+= $qty;
					$totalPrice = $totalPrice + $this->_objectManager->create('Ced\CsAramexshipping\Helper\Data')->getItemAmount($item) * $qty;
					$description .= $item->getProduct()->getName()."||";
				}
				 
			}

			$shipping = $this->_objectManager->get('Ced\CsOrder\Helper\Data')->isShipmentCreated($order);
				if(!$shipping)
					$totalPrice += $seller_order->getShippingAmount();

			if($order->getPayment()->getMethodInstance()->getCode() == 'cashondelivery'){
				$cod_amount = $totalPrice;

			}


			$params = ['Shipments' => [
							'Shipment' => [
									'Shipper' => [
											'Reference1' => $order->getIncrementId ().'-'.$vendorId,
											'AccountNumber' => $account_number,
											'PartyAddress' => [
													'Line1' => $store_street1,
													'Line2' => $store_street2,
													'City' => $store_city,
													'StateOrProvinceCode' => '',
													'PostCode' => $store_zip,
													'CountryCode' => $store_country 
											],
											'Contact' => [
													'Department' => '',
													'PersonName' => $vendor['name'],
													'Title' => '',
													'CompanyName' => $vendor['public_name'],
													'PhoneNumber1' => $vendor['contact_number'],
													'FaxNumber' => '',
													'CellPhone' => $vendor['contact_number'],
													'EmailAddress' => $vendor['email'],
													'Type' => '' 
											] 
									],
									
									'Consignee' => [
											'Reference1' => $order->getIncrementId ().'-'.$vendorId,
											'AccountNumber' => '',
											'PartyAddress' => [
													'Line1' => $shippingaddress->getStreet ()[0],
													'City' => $customer_city,
													'StateOrProvinceCode' => '',
													'PostCode' => $customer_postcode,
													'CountryCode' => $customer_country 
											],
											
											'Contact' => [
													'Department' => '',
													'PersonName' => $shippingaddress->getFirstname (),
													'Title' => '',
													'CompanyName' => $shippingaddress->getCompany (),
													'PhoneNumber1' => $shippingaddress->getTelephone (),
													'FaxNumber' => '',
													'CellPhone' => $shippingaddress->getTelephone (),
													'EmailAddress' => $shippingaddress->getEmail (),
													'Type' => '' 
											] 
									],
									
									'ThirdParty' => [
											'Reference1' => '',
											'Reference2' => '',
											'AccountNumber' => '',
											'PartyAddress' => [
													'Line1' => '',
													'Line2' => '',
													'Line3' => '',
													'City' => '',
													'StateOrProvinceCode' => '',
													'PostCode' => '',
													'CountryCode' => '' 
											],
											'Contact' => [
													'Department' => '',
													'PersonName' => '',
													'Title' => '',
													'CompanyName' => '',
													'PhoneNumber1' => '',
													'PhoneNumber1Ext' => '',
													'PhoneNumber2' => '',
													'PhoneNumber2Ext' => '',
													'FaxNumber' => '',
													'CellPhone' => '',
													'EmailAddress' => '',
													'Type' => '' 
											] 
									],
									
									'Reference1' => $order->getIncrementId ().'-'.$vendorId,
									'ForeignHAWB' => $order->getIncrementId () . rand ( 10, 100 ),
									'TransportType' => 0,
									'ShippingDateTime' => time (),
									'DueDate' => time (),
									'PickupLocation' => 'Reception',
									'PickupGUID' => '',
									'Comments' => $order->getIncrementId ().'-'.$vendorId,
									'AccountingInstrcutions' => '',
									'OperationsInstructions' => '',
									
									'Details' => [
											'ActualWeight' => [
													'Value' => $totalWeight,
													'Unit' => $weight_unit 
											],
											
											'ProductGroup' => $product_group,
											'ProductType' => $product_type,
											'PaymentType' => 'P',
											'PaymentOptions' => '',
											'Services' => '',
											'NumberOfPieces' => $totalItems,
											'DescriptionOfGoods' => $description,
											'GoodsOriginCountry' => $store_country,
											
											'CashOnDeliveryAmount' => [
													'Value' => $cod_amount,
													'CurrencyCode' => $this->_storeManager->getStore()->getCurrentCurrencyCode() 
											],
											
											'InsuranceAmount' => [
													'Value' => 0,
													'CurrencyCode' => '' 
											],
											
											'CollectAmount' => [
													'Value' => 0,
													'CurrencyCode' => '' 
											],
											
											'CashAdditionalAmount' => [
													'Value' => 0,
													'CurrencyCode' => '' 
											],
											
											'CashAdditionalAmountDescription' => '',
											
											'CustomsValueAmount' => [
													'Value' => 0,
													'CurrencyCode' => '' 
											],
											
											'Items' => []

											 
									] 
							] 
					],
			

					'ClientInfo' => [
							'AccountCountryCode' => $account_country_code,
							'AccountEntity' => $account_entity ,
							'AccountNumber' => $account_number ,
							'AccountPin' => $account_pin ,
							'UserName' => $account_username ,
							'Password' => $account_password ,
							'Version' => 'v1.0' 
					],
					
					'Transaction' => [
							'Reference1' => $order->getIncrementId ().'-'.$vendorId,
					],
					'LabelInfo' => [
							'ReportID' => 9201,
							'ReportType' => 'URL' 
					] 
			];
			

			if($order->getPayment()->getMethodInstance()->getCode() == 'cashondelivery'){
				$params['Shipments']['Shipment']['Details']['Services'] = 'CODS';
			}
			$params ['Shipments'] ['Shipment'] ['Details'] ['Items'] [] = [
					'PackageType' => 'Box',
					'Quantity' => $totalItems,
					'Weight' => [
							'Value' => $totalWeight,
							'Unit' => $weight_unit 
					],
					'Comments' => $description,
					'Reference' => '' 
			];

			$soapClient = new \SoapClient($wsdl);
			try{
				$auth_call = $soapClient->CreateShipments ( $params );
				
				if ($auth_call->HasErrors) {
					print_r($params);
					print_r($auth_call);die;
				}else{
					$awbno = $auth_call->Shipments->ProcessedShipment->ID;
					$shipment = $observer->getEvent()->getShipment();
	                $track = $this->_objectManager->create(
	                'Magento\Sales\Model\Order\Shipment\Track'
	                                            )->setNumber(
	                                                $awbno
	                                            )->setCarrierCode(
	                                                'aramexshipping'
	                                            )->setTitle(
	                                                'Aramex Shipping'
	                                            );
	                $shipment->addTrack($track);

				}
				
			}catch(\Exception $e){
				print_r($e->getMessage());die;
			}
		}       
    }
}
