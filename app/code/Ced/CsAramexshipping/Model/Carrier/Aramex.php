<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * You can check the licence at this URL: https://cedcommerce.com/license-agreement.txt
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @category    Ced
 * @package     Ced_CsAramexshipping
 * @author   CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright   Copyright CEDCOMMERCE (https://cedcommerce.com/)
 * @license     https://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Ced\CsAramexshipping\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Framework\Module\Dir;
use Magento\Framework\Xml\Security;

/**
 * Class Aramex
 * @package Ced\CsAramexshipping\Model\Carrier
 */
class Aramex extends \Ced\Aramexshipping\Model\Carrier\Aramex implements \Magento\Shipping\Model\Carrier\CarrierInterface
{

    const EXPRESS = 'EXP';

    const DOMESTIC = 'DOM';

    const PAYMENT_TYPE_PREPAID = 'P';

    const PRODT_ON_DELIVERY = 'OND';

    const CODE = 'aramexshipping';

    /**
     * @var string
     */
    protected $_code = self::CODE;

    protected $_request;

    protected $_result;

    protected $_baseCurrencyRate;

    protected $_xmlAccessRequest;

    protected $_localeFormat;

    protected $_logger;

    protected $configHelper;
    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    protected $_rateMethodFactory;
    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory
     */
    protected $_rateResultFactory;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var bool
     */
    protected $_isFixed = true;

    /**
     * Path to wsdl file of rate service
     *
     * @var string
     */
    protected $_rateServiceWsdl;

    /**
     * @var \Ced\CsMultiShipping\Helper\Data
     */
    protected $multishippingHelper;

    /**
     * @var \Ced\CsMarketplace\Model\VsettingsFactory
     */
    protected $vsettingsFactory;

    /**
     * Aramex constructor.
     * @param \Ced\CsMultiShipping\Helper\Data $multishippingHelper
     * @param \Ced\CsMarketplace\Model\VsettingsFactory $vsettingsFactory
     * @param \Ced\Aramexshipping\Model\System\Config\Source\Producttypes $producttypes
     * @param \Ced\Aramexshipping\Helper\Data $aramexshippingHelper
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param Security $xmlSecurity
     * @param \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory
     * @param \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory
     * @param \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Directory\Helper\Data $directoryData
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param Dir\Reader $configReader
     * @param array $data
     */
    public function __construct(
        \Ced\CsMultiShipping\Helper\Data $multishippingHelper,
        \Ced\CsMarketplace\Model\VsettingsFactory $vsettingsFactory,
        \Ced\Aramexshipping\Model\System\Config\Source\Producttypes $producttypes,
        \Ced\Aramexshipping\Helper\Data $aramexshippingHelper,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        Security $xmlSecurity,
        \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory,
        \Magento\Shipping\Model\Rate\ResultFactory $rateFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory,
        \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory,
        \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Directory\Helper\Data $directoryData,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        Dir\Reader $configReader,
        array $data = []
    )
    {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->scopeConfig = $scopeConfig;
        $this->producttypes = $producttypes;
        $this->checkoutSession = $checkoutSession;
        $this->multishippingHelper = $multishippingHelper;
        $this->aramexshippingHelper = $aramexshippingHelper;
        $this->vsettingsFactory = $vsettingsFactory;
        parent::__construct(
            $producttypes,
            $aramexshippingHelper,
            $checkoutSession,
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $xmlSecurity,
            $xmlElFactory,
            $rateFactory,
            $rateMethodFactory,
            $trackFactory,
            $trackErrorFactory,
            $trackStatusFactory,
            $regionFactory,
            $countryFactory,
            $currencyFactory,
            $directoryData,
            $stockRegistry,
            $rateResultFactory,
            $configReader,
            $data
        );

        $wsdlBasePath = $configReader->getModuleDir(Dir::MODULE_ETC_DIR, 'Ced_CsAramexshipping') . '/wsdl/';
        $this->_rateServiceWsdl = $wsdlBasePath . 'aramex_rates_calculator_service.wsdl';
    }

    /**
     * Create soap client with selected wsdl
     *
     * @param string $wsdl
     * @param bool|int $trace
     * @return \SoapClient
     */
    protected function _createSoapClient($wsdl, $trace = false)
    {
        $client = new \SoapClient($wsdl, ['trace' => $trace]);
        return $client;
    }


    /**
     * Create rate soap client
     *
     * @return \SoapClient
     */
    protected function _createRateSoapClient()
    {
        return $this->_createSoapClient($this->_rateServiceWsdl);
    }

    /**
     * @param RateRequest $request
     * @return bool|\Magento\Framework\DataObject|\Magento\Shipping\Model\Rate\Result|void|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->scopeConfig->getValue('carriers/aramexshipping/active',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE))
            return;

        if (!$this->multishippingHelper->isEnabled()) {
            return parent::collectRates($request);
        }
        

        $vendorId = $request->getVendorId();
        if (!$vendorId)
            return;
        $vsetting_model = $this->vsettingsFactory->create();
        $aramexSpecificConfig = array();
        $availableCountries = array();
        $vendor = array();

        $result = $this->_rateResultFactory->create();

        if ($vendorId != "admin")
            $aramexSpecificConfig = $request->getVendorShippingSpecifics();

        if ($vendorId != "admin") {
            $storeAdd = $vsetting_model->getCollection()->addFieldToFilter('vendor_id', $vendorId)->getData();
            foreach ($storeAdd as $key => $value) {
                if ($value['key'] == 'shipping/address/country_id') {
                    $cId = $value['value'];
                }
                if ($value['key'] == 'shipping/address/postcode') {
                    $pCode = $value['value'];
                }
                if ($value['key'] == 'shipping/address/city') {
                    $city = $value['value'];
                }
                if ($value['key'] == 'shipping/address/region') {
                    $regioncode = $value['value'];
                }
            }
            $aramexSpecificConfig = $request->getVendorShippingSpecifics();
            $allowed_methods = $this->producttypes->toKeyArray();
            $vendor_allowed_methods = explode(',', $aramexSpecificConfig['allowed_method']);
            $vendor_allowed_methods = array_flip($vendor_allowed_methods);
            $allowed_methods = array_intersect_key($allowed_methods, $vendor_allowed_methods);
            $destinationData = array(
                'StateOrProvinceCode' => $request->getDestRegionCode(),
                'City' => $request->getDestCity(),
                'CountryCode' => $request->getDestCountryId(),
                'PostCode' => $request->getDestPostcode()
            );

            $quoteId = $this->checkoutSession->getQuoteId();
            $items = $request->getAllItems();
            $weight = 0;
            $quantity = 0;
            foreach ($items as $item) {
                $quantity += $item->getQty();
                $weight += $item->getWeight() * $item->getQty();
            }
            $requestData = array(
                'packageWeight' => $weight,
                'packageQty' => $quantity
            );
            $productGroup = $request->getDestCountryId() == $cId ?
                self::DOMESTIC : self::EXPRESS;
            $productType = $productGroup == self::DOMESTIC ?
                self::PRODT_ON_DELIVERY : $this->getConfigData('product_type');
            $params = array(
                'ClientInfo' => array(
                    'AccountCountryCode' => $this->getConfigData('account_country_code'),
                    'AccountEntity' => $this->getConfigData('account_entity'),
                    'AccountNumber' => $this->getConfigData('account_number'),
                    'AccountPin' => $this->getConfigData('account_pin'),
                    'UserName' => $this->getConfigData('username'),
                    'Password' => $this->getConfigData('password'),
                    'Version' => 'v1.0'
                ),

                'Transaction' => array(
                    'Reference1' => '001'
                ),

                'OriginAddress' => array(
                    'StateOrProvinceCode' => $regioncode,
                    'City' => $city,
                    'CountryCode' => $cId,
                ),

                'DestinationAddress' => $destinationData,
                'ShipmentDetails' => array(
                    'PaymentType' => self::PAYMENT_TYPE_PREPAID,
                    'ProductGroup' => $productGroup,
                    'ProductType' => '',
                    'ActualWeight' => array('Value' => $requestData['packageWeight'],
                        'Unit' => $this->getConfigData('unit_of_measure')),
                    'ChargeableWeight' => array('Value' => $requestData['packageWeight'],
                        'Unit' => $this->getConfigData('unit_of_measure')),
                    'NumberOfPieces' => $requestData['packageQty']
                )
            );
            $soapClient = $this->_createRateSoapClient();
            foreach ($allowed_methods as $m_value => $m_title) {
                $params['ShipmentDetails']['ProductType'] = $m_value;
                try {
                    $results = $soapClient->CalculateRate($params);
                    // print_r($params);
                    // echo '--------------------';
                    // print_r($results);
                    // echo '++++++++++++++++++++';
                    $currentcurrency = $results->TotalAmount->CurrencyCode;
                    $price = $results->TotalAmount->Value;
                    $getprice = $this->aramexshippingHelper
                        ->convertRateCurrency($price, $currentcurrency);
                        
                    if ($results->HasErrors) {
                        $response['type'] = 'error';
                    } else {
                        $response['type'] = 'success';
                        $priceArr[$m_value] = array('label' => $m_title, 'amount' => $getprice['price']);

                    }
                } catch (\Exception $e) {
                    $response['type'] = 'error';
                    $response['error'] = $e->getMessage();
                }
            }
            //die('aaaaaaa');
            $result = $this->_rateResultFactory->create();
            $defaults = $this->getDefaults();
            if (empty($priceArr)) {
                $error = $this->_rateErrorFactory->create();
                $error->setCarrier($this->_code);
                $error->setCarrierTitle($this->getConfigData('title'));
                $error->setErrorMessage($this->getConfigData('specificerrmsg'));
                $result->append($error);
            } else {
                foreach ($priceArr as $method => $values) {
                    $rate = $this->_rateMethodFactory->create();
                    $rate->setVendorId($vendorId);
                    $rate->setCarrier($this->_code);
                    $rate->setCarrierTitle($this->getConfigData('title'));
                    $custom_method = $method . \Ced\CsMultiShipping\Model\Shipping::SEPARATOR . $vendorId;
                    $rate->setMethod($custom_method);
                    $rate->setMethodTitle('Aramex - ' . $values['label']);
                    $rate->setCost($values['amount']);
                    $rate->setPrice($values['amount']);
                    $result->append($rate);
                }
            }

            return $result;
        } else {
            return parent::collectRates($request);
        }
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {

        return [$this->_code => $this->getConfigData('name')];
    }


    /**
     * @param $trackings
     * @return mixed
     */
    public function getTracking($trackings)
    {
        if (!is_array($trackings)) {
            $trackings = [$trackings];
        }
        $this->_getXmlTrackingInfo($trackings);
        return $this->_result;
    }

    /**
     * @param $trackings
     * @return \Magento\Shipping\Model\Tracking\Result
     */
    public function _getXmlTrackingInfo($trackings)
    {
        $result = $this->_trackFactory->create();
        $title = $this->_scopeConfig->getValue('carriers/aramexshipping/title',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $track_url = $this->_scopeConfig->getValue('carriers/aramexshipping/track_url',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        foreach ($trackings as $tracking) {
            $status = $this->_trackStatusFactory->create();
            $status->setCarrier($this->_code);
            $status->setCarrierTitle($title);
            $status->setTracking($tracking);
            $status->setPopup(1);
            $status->setUrl("{$track_url}={$tracking}");
            $result->append($status);
        }
        $this->_result = $result;
        return $result;
    }

    /**
     * @param \Magento\Framework\DataObject $request
     * @return $this
     */
    public function processAdditionalValidation(\Magento\Framework\DataObject $request) {
        return $this;
    }

    /**
     * @param \Magento\Framework\DataObject $request
     * @return $this
     */
    public function proccessAdditionalValidation(\Magento\Framework\DataObject $request) {
        return $this;
    }

}