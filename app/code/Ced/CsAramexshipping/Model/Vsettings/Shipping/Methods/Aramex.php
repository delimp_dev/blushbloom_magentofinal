<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * You can check the licence at this URL: https://cedcommerce.com/license-agreement.txt
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @category    Ced
 * @package     Ced_CsAramexshipping
 * @author   CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright   Copyright CEDCOMMERCE (https://cedcommerce.com/)
 * @license     https://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Ced\CsAramexshipping\Model\Vsettings\Shipping\Methods;

use Ced\DomesticAustralianShipping\Helper\Config;
use Magento\Framework\Api\AttributeValueFactory;

/**
 * Class Aramex
 * @package Ced\CsAramexshipping\Model\Vsettings\Shipping\Methods
 */
class Aramex extends \Ced\CsMultiShipping\Model\Vsettings\Shipping\Methods\AbstractModel
{
    /**
     * @var string
     */
    protected $_code = 'aramexshipping';
    /**
     * @var array
     */
    protected $_fields = array();
    /**
     * @var string
     */
    protected $_codeSeparator = '-';

    /**
     * @var \Ced\CsAramexshipping\Model\System\Config\Source\Producttypes
     */
    protected $producttypes;

    /**
     * Aramex constructor.
     * @param \Ced\CsAramexshipping\Model\System\Config\Source\Producttypes $producttypes
     * @param \Ced\CsMarketplace\Helper\Data $csmarketplaceHelper
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Ced\CsAramexshipping\Model\System\Config\Source\Producttypes $producttypes,
        \Ced\CsMarketplace\Helper\Data $csmarketplaceHelper,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        $this->producttypes = $producttypes;
        parent::__construct(
            $csmarketplaceHelper,
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * @return array|mixed
     */
    public function getFields()
    {
        $fields['active'] = array('type' => 'select',
            'required' => true,
            'values' => array(
                array('label' => __('Yes'), 'value' => 1),
                array('label' => __('No'), 'value' => 0)
            )
        );


        $alloptions = $this->producttypes->toOptionArray();
        $fields['allowed_method'] = array('type' => 'multiselect',
            'required' => true,
            'values' => $alloptions
        );

        $unitofmeasure = $this->producttypes->toOptionArray();
        $fields['unit'] = array('type' => 'select',
            'required' => true,
            'values' => $unitofmeasure
        );


        return $fields;
    }

    /**
     * Retreive labels
     *
     * @param string $key
     * @return string
     */
    public function getLabel($key)
    {
        switch ($key) {
            case 'label' :
                return __('Aramex Shipping');
                break;
            case 'allowed_method':
                return __('Allowed Method');
                break;
            case 'active':
                return __('Active');
                break;
            case 'unit':
                return __('Unit Of Measure');
                break;
            default :
                return parent::getLabel($key);
                break;
        }
    }

}
			