<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * You can check the licence at this URL: https://cedcommerce.com/license-agreement.txt
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @category    Ced
 * @package     Ced_CsAramexshipping
 * @author   CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright   Copyright CEDCOMMERCE (https://cedcommerce.com/)
 * @license     https://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Ced\CsAramexshipping\Helper;

/**
 * Class Data
 * @package Ced\CsAramexshipping\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Ced\CsMarketplace\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Directory\Model\CurrencyFactory
     */
    protected $currencyFactory;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Ced\CsMarketplace\Helper\Data $markeplaceHelper
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Ced\CsMarketplace\Helper\Data $markeplaceHelper,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory
    )
    {
        $this->_storeManager = $storeManager;
        parent::__construct($context);
        $this->_helper = $markeplaceHelper;
        $this->currencyFactory = $currencyFactory;
    }

    /**
     * @param int $storeId
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function isEnabled($storeId = 0)
    {
        if ($storeId == 0)
            $storeId = $this->_helper->getStore()->getId();
        return $this->_helper->getStoreConfig('ced_csaramexshipping/general/active', $storeId);
    }

    /**
     * Convert currency
     *
     */
    public function currencyConvert($price, $from, $to, $output = '', $round = null)
    {
        $from = strtoupper($from);
        $to = strtoupper($to);

        $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrencyCode();
        $currentCurrencyCode = $this->_storeManager->getStore()->getCurrentCurrencyCode();

        if ('_BASE_' == $from) {
            $from = $baseCurrencyCode;
        } elseif ('_CURRENT_' == $from) {
            $from = $currentCurrencyCode;
        }

        if ('_BASE_' == $to) {
            $to = $baseCurrencyCode;
        } elseif ('_CURRENT_' == $to) {
            $to = $currentCurrencyCode;
        }

        $output = strtolower($output);

        $error = false;
        $result = array('price' => $price, 'currency' => $from);

        if ($from != $to) {
            $allowedCurrencies = $this->currencyFactory->create()->getConfigAllowCurrencies();
            $rates = $this->currencyFactory->create()->getCurrencyRates($baseCurrencyCode, array_values($allowedCurrencies));

            if (empty($rates) || !isset($rates[$from]) || !isset($rates[$to])) {
                $error = true;
            } elseif (empty($rates[$from]) || empty($rates[$to])) {
                $error = true;
            }

            if ($error) {
                if (isset($result[$output])) {
                    return $result[$output];
                } else {
                    return $result;
                }
            }

            $result = array(
                'price' => ($price * $rates[$to]) / $rates[$from],
                'currency' => $to
            );
        }

        if (is_int($round)) {
            $result['price'] = round($result['price'], $round);
        }

        if (isset($result[$output])) {
            return $result[$output];
        }

        return $result;
    }

    /**
     * @param $price
     * @param $currentcurrency
     * @return array|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function convertRateCurrency($price, $currentcurrency)
    {
        $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrencyCode();
        $result = array('price' => $price, 'currency' => $currentcurrency);

        if ($currentcurrency != $baseCurrencyCode) {
            $result = $this->currencyConvert($price, $currentcurrency, $baseCurrencyCode);
        }
        return $result;
    }
}
	