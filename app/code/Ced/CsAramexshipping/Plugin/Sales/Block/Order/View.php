<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_Aramexshipping
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (https://cedcommerce.com/)
 * @license      https://cedcommerce.com/license-agreement.txt
 */

namespace Ced\CsAramexshipping\Plugin\Sales\Block\Order;

use Magento\Shipping\Block\Adminhtml\View as ShipmentView;
use Magento\Framework\UrlInterface;

/**
 * Class View
 * @package Ced\CsAramexshipping\Plugin\Sales\Block\Order
 */
class View
{
    /**
     * @var UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var \Magento\Sales\Model\Order\ShipmentFactory
     */
    protected $shipmentFactory;

    /**
     * View constructor.
     * @param UrlInterface $url
     * @param \Magento\Sales\Model\Order\ShipmentFactory $shipmentFactory
     */
    public function __construct(
        UrlInterface $url,
        \Magento\Sales\Model\Order\ShipmentFactory $shipmentFactory
    )
    {
        $this->_urlBuilder = $url;
        $this->shipmentFactory = $shipmentFactory;
    }

    /**
     * @param ShipmentView $view
     */
    public function beforeSetLayout(ShipmentView $view)
    {
        $shipment_id = $view->getRequest()->getParam('shipment_id');
        $shipment = $this->shipmentFactory->create()->load($shipment_id);
        $shipping_method = $shipment->getOrder()->getShippingMethod();
        if (strpos($shipping_method, 'aramexshipping') !== false) {
            $url = $this->_urlBuilder->getUrl('csaramex/shipment/print', ['id' => $shipment_id]);

            $view->addButton(
                'aramex_label',
                [
                    'label' => __('Aramex Label'),
                    'class' => 'aramex-label',
                    'target' => '_blank',
                    'onclick' => 'window.open(\'' . $url . '\')'
                ]
            );
        }
    }
}