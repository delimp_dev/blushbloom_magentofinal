<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */

/**
 * Copyright В© 2016 Solar. All rights reserved.
 */
namespace Solar\Customform\Block\Widget\Form\Element;

class Listbox extends Dropdown
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('ListBox');
        $this->options['image_href'] = 'Solar_Customform::images/listbox.png';
    }

    public function generateContent()
    {
        return '<select class="select multiselect admin__control-multiselect" multiple="multiple"><option value="">'
            . $this->getTestOptionText()
            . '</option></select>';
    }
}
