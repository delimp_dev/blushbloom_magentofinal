<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */

/**
 * Copyright В© 2016 Solar. All rights reserved.
 */
namespace Solar\Customform\Block\Widget\Form\Element;

class File extends AbstractElement
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('File');
        $this->options['image_href'] = 'Solar_Customform::images/upload.png';
    }

    public function generateContent()
    {
        return '<input class="form-control" type="file"/>';
    }
}
