<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */


namespace Solar\Customform\Block\Widget\Form\Element;

class Text extends AbstractElement
{
    public function _construct()
    {
        parent::_construct();

        $this->options['title'] = __('Text');
        $this->options['image_href'] = 'Solar_Customform::images/text.png';
    }

    public function generateContent()
    {
        return '<div>' . $this->getExamplePhrase() . '</div>';
    }

    public function getExamplePhrase()
    {
        return __('Sphinx of black quartz, judge my vow');
    }
}
