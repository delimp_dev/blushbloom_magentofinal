<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */


namespace Solar\Customform\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface AnswerRepositoryInterface
 * @api
 */
interface AnswerRepositoryInterface
{
    /**
     * @param \Solar\Customform\Api\Data\AnswerInterface $answer
     * @return \Solar\Customform\Api\Data\AnswerInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Solar\Customform\Api\Data\AnswerInterface $answer);

    /**
     * @param int $answerId
     * @return \Solar\Customform\Api\Data\AnswerInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($answerId);

    /**
     * @param \Solar\Customform\Api\Data\AnswerInterface $answer
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(\Solar\Customform\Api\Data\AnswerInterface $answer);

    /**
     * @param int $answerId
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($answerId);

    /**
     * Lists by criteria
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Solar\Customform\Api\Data\AnswerInterface[] Array of items.
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getListFilter(SearchCriteriaInterface $searchCriteria);

    /**
     * Lists
     *
     * @return \Solar\Customform\Api\Data\AnswerInterface[] Array of items.
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getList();
}
