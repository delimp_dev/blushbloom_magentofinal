<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */


namespace Solar\Customform\Controller\Adminhtml\Forms;

class Index extends \Solar\Customform\Controller\Adminhtml\Forms
{
    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE_FORMS);
        $resultPage->addBreadcrumb(__('Content'), __('Content'));
        $resultPage->addBreadcrumb(__('Manage Custom Forms'), __('Manage Custom Forms'));
        $resultPage->getConfig()->getTitle()->prepend(__('Custom Forms'));

        return $resultPage;
    }
}
