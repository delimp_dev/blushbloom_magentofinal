<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */


namespace Solar\Customform\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var Operation\AddEmailResponse
     */
    private $addEmailResponse;

    /**
     * @var Operation\AddResponseStatus
     */
    private $addResponseStatus;

    /**
     * @var Operation\AddPopupColumns
     */
    private $addPopupColumns;

    /**
     * @var Operation\AddTitlesField
     */
    private $addTitlesField;

    public function __construct(
        \Solar\Customform\Setup\Operation\AddEmailResponse $addEmailResponse,
        Operation\AddResponseStatus $addResponseStatus,
        Operation\AddPopupColumns $addPopupColumns,
        Operation\AddTitlesField $addTitlesField
    ) {
        $this->addEmailResponse = $addEmailResponse;
        $this->addResponseStatus = $addResponseStatus;
        $this->addPopupColumns = $addPopupColumns;
        $this->addTitlesField = $addTitlesField;
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.5.0', '<')) {
            $this->addEmailResponse->execute($setup);
        }

        if (version_compare($context->getVersion(), '1.7.0', '<')) {
            $this->addResponseStatus->execute($setup);
        }

        if (version_compare($context->getVersion(), '1.8.0', '<')) {
            $this->addPopupColumns->execute($setup);
        }

        if (version_compare($context->getVersion(), '1.9.0', '<')) {
            $this->addTitlesField->execute($setup);
        }

        $setup->endSetup();
    }
}
