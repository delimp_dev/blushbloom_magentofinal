<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Rules
 */


namespace Solar\Rules\Api;

interface RuleProviderInterface
{
    /**
     * @param int $ruleId
     *
     * @return \Solar\Rules\Model\Rule
     */
    public function getAmruleByRuleId($ruleId);
}
