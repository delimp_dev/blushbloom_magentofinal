<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Rules
 */


namespace Solar\Rules\Model\Rule\Action\Discount;

use Magento\Quote\Model\Quote\Item\AbstractItem;
use Magento\SalesRule\Model\Rule;
use Magento\SalesRule\Model\Rule\Action\Discount\Data;

/**
 * Solar Rules calculation by action.
 *
 * @see \Solar\Rules\Helper\Data::TYPE_XN_FIXDISC
 */
class BuyxgetnFixdisc extends Buyxgety
{
    const RULE_VERSION = '1.0.0';

    /**
     * @param Rule $rule
     * @param AbstractItem $item
     * @param float $qty
     *
     * @return Data
     *
     * @throws \Exception
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function calculate($rule, $item, $qty)
    {
        $this->beforeCalculate($rule);
        $discountData = $this->_calculate($rule, $item);
        $this->afterCalculate($discountData, $rule, $item);

        return $discountData;
    }

    /**
     * @param Rule $rule
     * @param AbstractItem $item
     *
     * @return Data
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _calculate($rule, $item)
    {
        /** @var Data $discountData */
        $discountData = $this->discountFactory->create();

        // no conditions for Y elements
        if (!$rule->getSolrrulesRule()->getPromoCats() && !$rule->getSolrrulesRule()->getPromoSkus()) {
            return $discountData;
        }

        $address = $item->getAddress();

        $triggerItems = $this->getTriggerElements($address, $rule);
        $realQty = $this->getTriggerElementQty($triggerItems);
        $maxQty = $this->getNQty($rule, $realQty);
        // find all allowed Y (discounted) elements and calculate total discount

        $passedItems = [];
        $lastId = 0;
        $currQty = 0;
        $allItems = $this->getSortedItems($address, $rule, self::DEFAULT_SORT_ORDER);
        $itemsId = $this->getItemsId($allItems);

        foreach ($allItems as $allItem) {
            if ($currQty >= $maxQty) {
                break;
            }

            // we always skip child items and calculate discounts inside parents
            if (!$this->canProcessItem($allItem, $triggerItems, $passedItems)) {
                continue;
            }

            // what should we do with bundles when we treat them as
            // separate items
            $passedItems[$allItem->getSolrrulesId()] = $allItem->getSolrrulesId();

            if (!$this->isDiscountedItem($rule, $allItem)) {
                continue;
            }

            $qty = $this->getItemQty($allItem);

            if (($qty == $currQty) && ($lastId == $item->getSolrrulesId())) {
                continue;
            }

            $qty = min($maxQty - $currQty, $qty);
            $currQty += $qty;

            if (in_array($item->getSolrrulesId(), $itemsId) && $allItem->getSolrrulesId() === $item->getSolrrulesId()) {
                $itemQty = $qty;
                $price = $this->validator->getItemPrice($allItem);
                $priceWithDiscount = $price - $allItem->getDiscountAmount() / $allItem->getQty();
                $discountAmount = $priceWithDiscount < $rule->getDiscountAmount()
                    ? $priceWithDiscount
                    : $rule->getDiscountAmount();

                $quoteAmount = $this->priceCurrency->convert($discountAmount, $item->getQuote()->getStore());
                $discountData->setAmount($itemQty * $quoteAmount);
                $discountData->setBaseAmount($itemQty * $discountAmount);
                $discountData->setOriginalAmount($itemQty * $quoteAmount);
                $discountData->setBaseOriginalAmount($itemQty * $discountAmount);
            }
        }

        return $discountData;
    }
}
