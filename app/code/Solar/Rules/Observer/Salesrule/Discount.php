<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Rules
 */


namespace Solar\Rules\Observer\Salesrule;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Entry point for @see \Solar\Rules\Model\DiscountRegistry::setDiscount
 */
class Discount implements ObserverInterface
{
    /**
     * @var \Solar\Rules\Model\DiscountRegistry
     */
    private $discountRegistry;

    public function __construct(
        \Solar\Rules\Model\DiscountRegistry $discountRegistry
    ) {
        $this->discountRegistry = $discountRegistry;
    }

    /**
     * @param Observer $observer
     * @return \Magento\SalesRule\Model\Rule\Action\Discount\Data|void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\SalesRule\Model\Rule $rule */
        $rule = $observer->getRule();
        /** @var \Magento\SalesRule\Model\Rule\Action\Discount\Data $result */
        $result = $observer->getResult();
        /** @var \Magento\Quote\Model\Quote $item */
        $item = $observer->getItem();

        $this->discountRegistry->setDiscount($result, $rule, $item);
    }
}
