<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2020 Solar (https://www.solar.com)
 * @package Solar_Base
 */


namespace Solar\Base\Model\AdminNotification\Model\ResourceModel\Inbox\Collection;

class Exists extends \Magento\AdminNotification\Model\ResourceModel\Inbox\Collection
{
    /**
     * @param \SimpleXMLElement $item
     * @return bool
     */
    public function execute(\SimpleXMLElement $item)
    {
        $this->addFieldToFilter('url', (string)$item->link);

        return $this->getSize() > 0;
    }
}
