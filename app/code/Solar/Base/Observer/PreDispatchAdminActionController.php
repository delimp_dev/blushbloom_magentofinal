<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2020 Solar (https://www.solar.com)
 * @package Solar_Base
 */


namespace Solar\Base\Observer;

use Magento\Framework\Event\ObserverInterface;

class PreDispatchAdminActionController implements ObserverInterface
{
    /**
     * @var \Solar\Base\Model\FeedFactory
     */
    private $feedFactory;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    private $backendSession;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    public function __construct(
        \Solar\Base\Model\FeedFactory $feedFactory,
        \Magento\Backend\Model\Auth\Session $backendAuthSession,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->feedFactory = $feedFactory;
        $this->backendSession = $backendAuthSession;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->backendSession->isLoggedIn()) {
            try {
                /** @var \Solar\Base\Model\Feed $feedModel */
                $feedModel = $this->feedFactory->create();

                $feedModel->checkUpdate();
                $feedModel->removeExpiredItems();
            } catch (\Exception $exception) {
                $this->logger->critical($exception);
            }
        }
    }
}
