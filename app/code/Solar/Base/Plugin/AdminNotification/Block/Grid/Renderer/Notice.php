<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2020 Solar (https://www.solar.com)
 * @package Solar_Base
 */


namespace Solar\Base\Plugin\AdminNotification\Block\Grid\Renderer;

use Magento\AdminNotification\Block\Grid\Renderer\Notice as NativeNotice;

class Notice
{
    public function aroundRender(
        NativeNotice $subject,
        \Closure $proceed,
        \Magento\Framework\DataObject $row
    ) {
        $result = $proceed($row);

        $solarLogo = '';
        $solarImage = '';
        if ($row->getData('is_solar')) {
            if ($row->getData('image_url')) {
                $solarImage = ' style="background: url(' . $row->getData("image_url") . ') no-repeat;"';
            } else {
                $solarLogo = ' solar-grid-logo';
            }
        }
        $result = '<div class="ambase-grid-message' . $solarLogo . '"' . $solarImage . '>' . $result . '</div>';

        return  $result;
    }
}
