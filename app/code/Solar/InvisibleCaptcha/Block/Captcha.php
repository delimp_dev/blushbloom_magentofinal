<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_InvisibleCaptcha
 */


namespace Solar\InvisibleCaptcha\Block;

class Captcha extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * Captcha model instance
     *
     * @var \Solar\InvisibleCaptcha\Model\Captcha
     */
    private $captchaModel;

    /**
     * Captcha constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context      $context
     * @param \Solar\InvisibleCaptcha\Model\Captcha                $captchaModel
     * @param array                                                 $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Solar\InvisibleCaptcha\Model\Captcha $captchaModel,
        array $data = []
    ) {
        $this->captchaModel = $captchaModel;
        parent::__construct($context, $data);
    }

    /**
     * Return Captcha model
     *
     * @return \Solar\InvisibleCaptcha\Model\Captcha
     */
    public function getCaptcha()
    {
        return $this->captchaModel;
    }

    /**
     * Produce and return block's html output
     *
     * @return string
     */
    public function toHtml()
    {
        if (!$this->getCaptcha()->isEnabled()) {
            return '';
        }
        return parent::toHtml();
    }
}
