# Excellence_Geoip v1.1.0
## Magento 2 Extension

### Magento 2 extension to automatically redirect and change currency based on customer's location


## Features:
1. Easily manage GeoIP based store switching
2. Map Country to Store View
3. Easily manage auto currency switching based on Country and Store View

___________________________________________________________________________________________________

### Screenshots:

<img src="https://i.ibb.co/ctByTXd/slide1.png" alt="Excellece Geoip Slider" title="Screenshot 1">

___________________________________________________________________________________________________

### Want to know more?

## Visit <a href='https://xmagestore.com/magento2/geoip-extension.html' target='_blank'>https://xmagestore.com/magento2/geoip-extension.html</a>
___________________________________________________________________________________________________
## Need any help with installation and setup?

## Visit <a href='http://wiki.xmagestore.com/Magento2/Geoip_Extension/index.html' target='_blank'>http://wiki.xmagestore.com/Magento2/Geoip_Extension/index.html</a>

___________________________________________________________________________________________________
## Prerequisites

### Use the following table to verify you have the correct prerequisites to install this Extension.
<table>
	<tbody>
		<tr>
			<th>Prerequisite</th>
			<th>How to check</th>
			<th>For more information</th>
		</tr>
	<tr>
		<td>Apache 2.2 or 2.4</td>
		<td>Ubuntu: <code>apache2 -v</code><br>
		CentOS: <code>httpd -v</code></td>
		<td><a href="https://devdocs.magento.com/guides/v2.2/install-gde/prereq/apache.html">Apache</a></td>
	</tr>
	<tr>
		<td>PHP 5.6.x, 7.0.2, 7.0.4 or 7.0.6</td>
		<td><code>php -v</code></td>
		<td><a href="http://devdocs.magento.com/guides/v2.2/install-gde/prereq/php-ubuntu.html">PHP Ubuntu</a><br><a href="http://devdocs.magento.com/guides/v2.2/install-gde/prereq/php-centos.html">PHP CentOS</a></td>
	</tr>
	<tr><td>MySQL 5.6.x</td>
	<td><code>mysql -u [root user name] -p</code></td>
	<td><a href="http://devdocs.magento.com/guides/v2.2/install-gde/prereq/mysql.html">MySQL</a></td>
	</tr>
</tbody>
</table>

___________________________________________________________________________________________________
### Feedback and Support <a href="mailto:support@xmagestore.com">support@xmagestore.com</a>
