<?php declare(strict_types=1);


namespace Category\Custom\Controller\Index;


class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\State $state,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->state = $state;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        echo "<pre>";
        $file = fopen("http://magentodev.segayah-alain.com/csv/products.csv","r");
        // $file = fopen('myCSVFile.csv', 'r');
        while (($line = fgetcsv($file)) !== FALSE) {
          //$line is an array of the csv elements
          print_r($line);
        }
        fclose($file);
        die;
        return $this->resultPageFactory->create();
    }
}

