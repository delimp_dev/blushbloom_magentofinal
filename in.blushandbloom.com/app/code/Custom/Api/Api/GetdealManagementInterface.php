<?php


namespace Custom\Api\Api;

/**
 * Interface GetdealManagementInterface
 *
 * @package Custom\Api\Api
 */
interface GetdealManagementInterface
{

    /**
     * GET for getdeal api
     * @param string $param
     * @return string
     */
    public function getGetdeal($param);
}

