<?php

namespace Infibeam\Ccavenue\Controller\Standard;

use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;

class Cancel extends \Infibeam\Ccavenue\Controller\CcavenueAbstract implements CsrfAwareActionInterface {

	public function createCsrfValidationException(
        RequestInterface $request
    ): ?InvalidRequestException {
        return null;
    }
    
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }

    public function execute() {
        $this->_cancelPayment('Payment has been cancelled');
        
        $this->messageManager->addErrorMessage(__('Your order has been cancelled'));
        $this->_logger->info('Payment has been cancelled.');

        $this->getResponse()->setRedirect(
                $this->getCheckoutHelper()->getUrl('checkout/cart')
        );
    }

}
