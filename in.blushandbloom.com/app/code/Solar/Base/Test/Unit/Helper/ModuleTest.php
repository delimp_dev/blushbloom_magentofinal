<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2020 Solar (https://www.solar.com)
 * @package Solar_Base
 */


namespace Solar\Base\Test\Unit\Helper;

use Solar\Base\Helper\Module;
use Solar\Base\Test\Unit\Traits;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Class ModuleTest
 *
 * @see Module
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * phpcs:ignoreFile
 */
class ModuleTest extends \PHPUnit\Framework\TestCase
{
    use Traits\ObjectManagerTrait;
    use Traits\ReflectionTrait;

    /**
     * @covers Module::getFeedModuleData
     */
    public function testGetFeedModuleData()
    {
        $helper = $this->createPartialMock(Module::class, ['getAllExtensions']);

        $helper->expects($this->any())->method('getAllExtensions')->willReturn([]);

        $this->assertEquals([], $helper->getFeedModuleData('test'));

        $this->setProperty($helper, 'modulesData', [['test1', 'test2']], Module::class);
        $this->assertEquals('test1', $helper->getFeedModuleData(0));
    }
}
