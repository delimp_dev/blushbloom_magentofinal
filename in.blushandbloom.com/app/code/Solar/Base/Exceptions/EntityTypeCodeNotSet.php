<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2020 Solar (https://www.solar.com)
 * @package Solar_Base
 */


namespace Solar\Base\Exceptions;

class EntityTypeCodeNotSet extends \Magento\Framework\Exception\LocalizedException
{
    /**
     * @param \Magento\Framework\Phrase $phrase
     * @param \Exception $cause
     * @param int $code
     */
    public function __construct(\Magento\Framework\Phrase $phrase = null, \Exception $cause = null, $code = 0)
    {
        if (!$phrase) {
            $phrase = __('Entity Type Code not set.');
        }
        parent::__construct($phrase, $cause, (int) $code);
    }
}
