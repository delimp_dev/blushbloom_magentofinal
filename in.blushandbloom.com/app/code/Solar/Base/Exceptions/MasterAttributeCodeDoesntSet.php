<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2020 Solar (https://www.solar.com)
 * @package Solar_Base
 */


namespace Solar\Base\Exceptions;

class MasterAttributeCodeDoesntSet extends \Magento\Framework\Exception\LocalizedException
{
    /**
     * @param \Magento\Framework\Phrase $phrase
     * @param \Exception $cause
     * @param int $code
     */
    public function __construct(\Magento\Framework\Phrase $phrase = null, \Exception $cause = null, $code = 0)
    {
        if (!$phrase) {
            $phrase = __('Master Attribute Code doesn\'t set.');
        }
        parent::__construct($phrase, $cause, (int) $code);
    }
}
