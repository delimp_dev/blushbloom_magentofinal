<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2020 Solar (https://www.solar.com)
 * @package Solar_Base
 */


namespace Solar\Base\Debug\System;

/**
 * @codeCoverageIgnore
 * @codingStandardsIgnoreFile
 */
class Template
{
    public static $varWrapper = '<div class="solar-base-debug-wrapper"><code>%s</code></div>';

    public static $string = '"<span class="solar-base-string">%s</span>"';

    public static $var = '<span class="solar-base-var">%s</span>';

    public static $arrowsOpened =  '<span class="solar-base-arrow" data-opened="true">&#x25BC;</span>
        <div class="solar-base-array">';

    public static $arrowsClosed = '<span class="solar-base-arrow" data-opened="false">&#x25C0;</span>
        <div class="solar-base-array solar-base-hidden">';

    public static $arrayHeader = '<span class="solar-base-info">array:%s</span> [';

    public static $array = '<div class="solar-base-array-line" style="padding-left:%s0px">
            %s  => %s
        </div>';

    public static $arrayFooter = '</div>]';

    public static $arrayKeyString = '"<span class="solar-base-array-key">%s</span>"';

    public static $arrayKey = '<span class="solar-base-array-key">%s</span>';

    public static $arraySimpleVar = '<span class="solar-base-array-value">%s</span>';

    public static $arraySimpleString = '"<span class="solar-base-array-string-value">%s</span>"';

    public static $objectHeader = '<span class="solar-base-info" title="%s">Object: %s</span> {';

    public static $objectMethod = '<div class="solar-base-object-method-line" style="padding-left:%s0px">
            #%s
        </div>';

    public static $objectMethodHeader = '<span style="margin-left:%s0px">Methods: </span>
        <span class="solar-base-arrow" data-opened="false">◀</span>
        <div class="solar-base-array  solar-base-hidden">';

    public static $objectMethodFooter = '</div>';

    public static $objectFooter = '</div> }';

    public static $debugJsCss = '<script>
            var solarToggle = function() {
                if (this.dataset.opened == "true") {
                    this.innerHTML = "&#x25C0";
                    this.dataset.opened = "false";
                    this.nextElementSibling.className = "solar-base-array solar-base-hidden";
                } else {
                    this.innerHTML = "&#x25BC;";
                    this.dataset.opened = "true";
                    this.nextElementSibling.className = "solar-base-array";
                }
            };
            document.addEventListener("DOMContentLoaded", function() {
                arrows = document.getElementsByClassName("solar-base-arrow");
                for (i = 0; i < arrows.length; i++) {
                    arrows[i].addEventListener("click", solarToggle,false);
                }
            });
        </script>
        <style>
            .solar-base-debug-wrapper {
                background-color: #263238;
                color: #ff9416;
                font-size: 13px;
                padding: 10px;
                border-radius: 3px;
                z-index: 1000000;
                margin: 20px 0;
            }
            .solar-base-debug-wrapper code {
                background: transparent !important;
                color: inherit !important;
                padding: 0;
                font-size: inherit;
                white-space: inherit;
            }
            .solar-base-info {
                color: #82AAFF;
            }
            .solar-base-var, .solar-base-array-key {
                color: #fff;
            }
            .solar-base-array-value {
                color: #C792EA;
                font-weight: bold;
            }
            .solar-base-arrow {
                cursor: pointer;
                color: #82aaff;
            }
            .solar-base-hidden {
                display:none;
            }
            .solar-base-string, .solar-base-array-string-value {
                font-weight: bold;
                color: #c3e88d;
            }
            .solar-base-object-method-line {
                color: #fff;
            }
        </style>';
}
