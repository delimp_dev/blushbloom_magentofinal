<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2020 Solar (https://www.solar.com)
 * @package Solar_Base
 */


namespace Solar\Base\Debug\System;

class SolarDump
{
    public $className = '';

    public $shortClassName = '';

    public $methods = [];

    public $properties = [];
}
