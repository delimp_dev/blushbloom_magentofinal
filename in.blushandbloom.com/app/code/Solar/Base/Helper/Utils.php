<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2020 Solar (https://www.solar.com)
 * @package Solar_Base
 */


namespace Solar\Base\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Utils extends AbstractHelper
{
    public function _exit($code = 0)
    {
        //phpcs:ignore
        exit($code);
    }

    public function _echo($a)
    {
        //phpcs:ignore
        echo $a;
    }
}
