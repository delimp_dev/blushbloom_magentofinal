<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2020 Solar (https://www.solar.com)
 * @package Solar_Base
 */


namespace Solar\Base\Model\Import\Behavior;

/**
 * @since 1.4.6
 */
interface BehaviorInterface
{
    /**
     * @param array $importData
     *
     * @return \Magento\Framework\DataObject|void
     */
    public function execute(array $importData);
}
