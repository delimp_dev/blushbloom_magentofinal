<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */


namespace Solar\Customform\Api;

/**
 * Interface FormRepositoryInterface
 * @api
 */
interface FormRepositoryInterface
{
    /**
     * @param \Solar\Customform\Api\Data\FormInterface $form
     * @return \Solar\Customform\Api\Data\FormInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Solar\Customform\Api\Data\FormInterface $form);

    /**
     * @param int $formId
     * @return \Solar\Customform\Api\Data\FormInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($formId);

    /**
     * @param string $formCode
     * @return Data\FormInterface|bool
     */
    public function getByFormCode($formCode);

    /**
     * @param \Solar\Customform\Api\Data\FormInterface $form
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(\Solar\Customform\Api\Data\FormInterface $form);

    /**
     * @param int $formId
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($formId);

    /**
     * Lists
     *
     * @return \Solar\Customform\Api\Data\FormInterface[] Array of items.
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getList();
}
