<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */


namespace Solar\Customform\Controller\Adminhtml\Answer;

use Solar\Customform\Api\Data\AnswerInterface;

class Index extends \Solar\Customform\Controller\Adminhtml\Answer
{
    public function execute()
    {
        if ($formId = (int)$this->getRequest()->getParam('form_id', null)) {
            $this->bookmark->applyFilter(
                'solar_customform_answer_listing',
                [
                    'form_id' => $formId,
                    AnswerInterface::ADMIN_RESPONSE_STATUS => $this->getRequest()->getParam('status', null)
                ]
            );
            $this->bookmark->clear();
        }

        $this->_initAction();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Submitted Data'));
        $this->_view->renderLayout();
    }
}
