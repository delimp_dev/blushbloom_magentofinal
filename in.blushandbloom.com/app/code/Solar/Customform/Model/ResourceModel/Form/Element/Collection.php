<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */


namespace Solar\Customform\Model\ResourceModel\Form\Element;

class Collection extends \Magento\Cms\Model\ResourceModel\Page\Collection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Solar\Customform\Model\Form\Element',
            'Solar\Customform\Model\ResourceModel\Form\Element'
        );
    }

    public function getFieldsByFormId($formId)
    {
        $this->addFieldToFilter('form_id', $formId);
        return $this;
    }
}
