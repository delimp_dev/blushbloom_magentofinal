<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */


namespace Solar\Customform\Model\ResourceModel\Form\Element\Option;

class Collection extends \Magento\Cms\Model\ResourceModel\Page\Collection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Solar\Customform\Model\Form\Element\Option',
            'Solar\Customform\Model\ResourceModel\Form\Element\Option'
        );
    }

    public function getFieldsByElementId($elementId)
    {
        $this->addFieldToFilter('element_id', $elementId);
        return $this;
    }
}
