<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */


namespace Solar\Customform\Model\ResourceModel\Answer;

class Collection extends \Magento\Cms\Model\ResourceModel\Page\Collection
{
    protected $_idFieldName = 'answer_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Solar\Customform\Model\Answer', 'Solar\Customform\Model\ResourceModel\Answer');
    }
}
