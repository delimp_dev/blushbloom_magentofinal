<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */


namespace Solar\Customform\Block\Widget\Form\Element;

class Textinput extends AbstractElement
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('Text Input');
    }

    public function generateContent()
    {
        return '<input class="form-control" type="text"/>';
    }
}
