<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */


namespace Solar\Customform\Block\Widget\Form\Element;

class Textarea extends Textinput
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('Textarea');
        $this->options['image_href'] = 'Solar_Customform::images/textarea.png';
    }

    public function generateContent()
    {
        return '<textarea class="form-control" type="text"/>';
    }
}
