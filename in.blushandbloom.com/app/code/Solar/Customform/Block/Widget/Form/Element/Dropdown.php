<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */

/**
 * Copyright В© 2016 Solar. All rights reserved.
 */
namespace Solar\Customform\Block\Widget\Form\Element;

class Dropdown extends AbstractElement
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('DropDown');
        $this->options['image_href'] = 'Solar_Customform::images/dropdown.png';
    }

    public function generateContent()
    {
        return '<select><option value="">' . $this->getTestOptionText() . '</option></select>';
    }

    protected function getTestOptionText()
    {
        return __('--Select an option--');
    }
}
