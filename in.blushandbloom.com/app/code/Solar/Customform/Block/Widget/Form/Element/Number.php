<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */


namespace Solar\Customform\Block\Widget\Form\Element;

class Number extends Textinput
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('Number Input');
        $this->options['image_href'] = 'Solar_Customform::images/textarea.png';
    }

    public function generateContent()
    {
        return '<input class="form-control" type="number"/>';
    }
}
