<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */

/**
 * Copyright В© 2016 Solar. All rights reserved.
 */
namespace Solar\Customform\Block\Widget\Form\Element;

class DateRange extends Date
{
    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('Date Range');
    }
}
