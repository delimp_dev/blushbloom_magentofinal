<?php
/**
 * @author Solar Team
 * @copyright Copyright (c) 2019 Solar (https://www.solar.com)
 * @package Solar_Customform
 */

/**
 * Copyright В© 2016 Solar. All rights reserved.
 */
namespace Solar\Customform\Block\Widget\Form\Element;

class Date extends AbstractElement
{
    /**
     * @var \Solar\Customform\Helper\Data
     */
    private $helper;

    public function __construct(\Solar\Customform\Helper\Data $helper)
    {
        parent::__construct();
        $this->helper = $helper;
    }

    public function _construct()
    {
        parent::_construct();
        $this->options['title'] = __('Date');
        $this->options['image_href'] = 'Solar_Customform::images/date.png';
    }

    /**
     * @inheritdoc
     */
    public function generateContent()
    {
        return '<input class="form-control" type="date"/>';
    }

    /**
     * @inheritdoc
     */
    public function getInputFormat()
    {
        return $this->helper->getDateFormat();
    }
}
