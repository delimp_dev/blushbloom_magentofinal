<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Telrapi\Payment\Model;

class PaymentManagement implements \Telrapi\Payment\Api\PaymentManagementInterface
{
	public function __construct(\Magento\Store\Model\StoreManagerInterface $storeManager
	)
	{
			$this->_storeManager = $storeManager;
	}

    /**
     * {@inheritdoc}
     */
    public function postPayment($param)
    {
    	$baseUrl = $this->_storeManager->getStore()->getBaseUrl();
    	$entityBodyJson = file_get_contents('php://input');
    	$entityBody = json_decode($entityBodyJson, true);
    	$ivp_currency = $entityBody['ivp_currency'];
    	$ivp_amount = $entityBody['ivp_amount'];
    	$bill_fname = $entityBody['bill_fname'];
    	$bill_sname = $entityBody['bill_sname'];
    	$bill_addr1 = $entityBody['bill_addr1'];
    	$bill_city = $entityBody['bill_city'];
    	$bill_region = $entityBody['bill_region'];
    	$bill_country = $entityBody['bill_country'];
    	$delv_zip = $entityBody['delv_zip'];
    	$bill_email = $entityBody['bill_email'];
    	$bill_phone1 = $entityBody['bill_phone1'];
    	$ivpCart = $entityBody['order_id'].'_'.(string)time();
    	$return_auth = $baseUrl.'telr/standard/response/?coid='.'$entityBody["order_id"]';
    	$return_can = $baseUrl.'telr/standard/cancel/';
    	$return_decl = $baseUrl.'telr/standard/cancel/';
    	$ivp_update_url = $baseUrl.'telr/standard/IvpCallback/?cart_id='.'$entityBody["order_id"]';
    	$params['ivp_method'] = 'create';
	    $params['ivp_store'] = 23983;
	    $params['ivp_authkey'] = 'zHWJ-WFMnJ~Scz2z';
	    $params['ivp_desc'] = 'payment';
	    $params['ivp_test'] = 1;
	    $params['ivp_source'] = '2.3.4';
	    $params['ivp_cart'] = $ivpCart;
	    $params['ivp_currency'] = $ivp_currency;
	    $params['ivp_amount'] = $ivp_amount;
	    $params['bill_fname'] = $bill_fname;
	    $params['bill_sname'] = $bill_sname;
	    $params['bill_addr1'] = $bill_addr1;
	    $params['ivp_framed'] = 0;
	    $params['ivp_lang'] = 'en';
	    $params['bill_city'] = $bill_city;
	    $params['bill_region'] = $bill_region;
	    $params['delv_zip'] = $delv_zip;
	    $params['bill_country'] = $bill_country;
	    $params['bill_email'] = $bill_email;
	    $params['bill_phone1'] = $bill_phone1;
	    $params['return_auth'] = $return_auth;
	    $params['return_can'] = $return_can;
	    $params['return_decl'] = $return_decl;
	    $params['ivp_update_url'] = $ivp_update_url;
	    $api_url = 'https://secure.telr.com/gateway/order.json';

    	$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api_url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        $returnData = json_decode(curl_exec($ch),true);
        curl_close($ch);
		// $statusArray = array('status' => 1, 'url' => $returnData['order']['url']);
		$statusArray['status'] = 1;
		$statusArray['url'] = $returnData['order']['url'];
			$jsonstatus = json_encode($statusArray);
			echo $jsonstatus;

		exit;
        
    }
}

